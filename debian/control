Source: pmount
Section: utils
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               intltool,
               libblkid-dev,
               libglib2.0-dev
Standards-Version: 4.5.0
Rules-Requires-Root: binary-targets
Homepage: https://alioth-archive.debian.org/release/pmount/pmount
Vcs-Browser: https://salsa.debian.org/debian/pmount
Vcs-Git: https://salsa.debian.org/debian/pmount.git

Package: pmount
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: cryptsetup (>= 1.0)
Description: mount removable devices as normal user
 pmount is a wrapper around the standard mount program which permits normal
 users to mount removable devices without a matching /etc/fstab entry. This
 provides a robust basis for automounting frameworks like GNOME's Utopia
 project and confines the amount of code that runs as root to a minimum.
 .
 If a LUKS capable cryptsetup package is installed, pmount is able to
 transparently mount encrypted volumes.
